package br.univel.cadastro;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Cadastro implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// atributos da Classe  "Cadastro"
	@Ann_Cadastro
	private int id;
	@Ann_Cadastro
	private String nome;
	@Ann_Cadastro
	private String telefone;
	@Ann_Cadastro
	private String endereco;
	
	
	public Cadastro(){}
	
	
	// Construtor da Classe Cadastro
	public Cadastro(int id, String nome, String telefone, String endereco) {
		super();
		this.id = id;
		this.nome = nome;
		this.telefone = telefone;
		this.endereco = endereco;
	}
	
	//m�todo GetID para capturar o ID
	public int getId() {
		return id;
	}
	//m�todo SetID para setar o ID
	public void setId(int id) {
		this.id = id;
	}
	//m�todo GetNome para capturar o Nome
	public String getNome() {
		return nome;
	}
	//m�todo SetNome para setar o Nome
	public void setNome(String nome) {
		this.nome = nome;
	}
	//m�todo GetTelefone para capturar o Telefone
	public String getTelefone() {
		return telefone;
	}
	//m�todo SetTelefone para setar o Telefone
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	//m�todo GetEndereco para capturar o Endereco
	public String getEndereco() {
		return endereco;
	}
	//m�todo SetEndereco para setar o Endereco
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
}
