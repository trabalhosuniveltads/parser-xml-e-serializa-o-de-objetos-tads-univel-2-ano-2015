package br.univel.cadastro;

import java.sql.SQLException;
import java.util.List;

public interface CadastroDao {
	public void abrirConexao() throws SQLException;
	public void fecharConexao() throws SQLException;
	public void criar(Cadastro c) throws SQLException;
	public List<Cadastro> ler() throws SQLException;
	public void atualizar(Cadastro c) throws SQLException;
	public void deletar(Cadastro c) throws SQLException;
}
