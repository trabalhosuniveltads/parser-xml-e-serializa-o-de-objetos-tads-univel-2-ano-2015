package br.univel.cadastro;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Cadastros {

	private ArrayList<Cadastro> cadastros = new ArrayList<Cadastro>();
	 
	public Cadastros() {}

	public ArrayList<Cadastro> getCadastros() {
		return cadastros;
	}

	public void setCadastros(ArrayList<Cadastro> cadastros) {
		this.cadastros = cadastros;
	}

}
