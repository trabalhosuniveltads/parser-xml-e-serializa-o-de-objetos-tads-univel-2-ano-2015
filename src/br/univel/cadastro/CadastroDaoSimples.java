package br.univel.cadastro;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class CadastroDaoSimples implements CadastroDao {
	private Connection con;
	
	@Override
	public void abrirConexao() throws SQLException {
		// prepara a conex�o com o banco de dados H2
		String url = "jdbc:h2:~/javacadastro";
		String user = "sa";
		String pass = "sa";
		con = DriverManager.getConnection(url, user, pass);
	}
	
	@Override
	public void fecharConexao() throws SQLException {
		//checha a conex�o
		con.close();
	}
	
	@Override
	public void criar(Cadastro c) throws SQLException {
		abrirConexao();
		//prepara o statement com o c�digo sql
		PreparedStatement ps = con.prepareStatement("INSERT INTO CADASTRO (ID, NOME, TELEFONE, ENDERECO) VALUES (?, ?, ?, ?)");
		
		//atribui os valores
		ps.setInt(1, c.getId());
		ps.setString(2, c.getNome());
		ps.setString(3, c.getTelefone());
		ps.setString(4, c.getEndereco());
		
		//executando o comando SQL
		ps.executeUpdate();
		
		//fechando a statement
		ps.close();
		//encerra a conexao
		fecharConexao();
	}
	
	@Override
	public ArrayList<Cadastro>ler() throws SQLException {
		//gera uma lista temporaria
		ArrayList<Cadastro> lista = new ArrayList<Cadastro>();
		
		//abre a conexao
		abrirConexao();
		
		// gera um estatement para a conexao
		Statement st = con.createStatement();
		// coleta os resultados
		ResultSet result = st.executeQuery("SELECT * FROM CADASTRO");
		
		//percorre os resultados
		while (result.next()) {
			//coleta os dados dos resultados em um modelo Cadastro
			Cadastro c = new Cadastro(result.getInt(1), result.getString(2), result.getString(3), result.getString(4));
			//adiciona o modelo na lista
			lista.add(c);
		}
		
		// encerra a conex�o
		fecharConexao();
		//retorna os resultados em forma de um arraylist
		return lista;
	}
	
	@Override
	public void atualizar(Cadastro c) throws SQLException {
		
		//abre a conex�o
		abrirConexao();
		//prepara a sql 
		PreparedStatement sql = con.prepareStatement("UPDATE CADASTRO SET ID = ?, NOME = ?, TELEFONE = ?, ENDERECO = ? WHERE ID = ?");
		//atribui os valores
		sql.setInt(1, c.getId());
		sql.setString(2, c.getNome());
		sql.setString(3, c.getTelefone());
		sql.setString(4, c.getEndereco());
		sql.setInt(5, c.getId());
		
		//executando o comando SQL
		sql.executeUpdate();
		
		//  finaliza
		sql.close();
		
		// encerra a conex�o
		fecharConexao();
	}
	
	@Override
	public void deletar(Cadastro c) throws SQLException {
		
		// abre a conex�o
		abrirConexao();
		
		// prepara a qsl
		PreparedStatement sql = con.prepareStatement("DELETE FROM CADASTRO WHERE ID = ?");
		
		//atribui os valores
		sql.setInt(1, c.getId());
		
		//executando o comando SQL
		sql.executeUpdate();
		
		// executa e encerra
		sql.close();
		
		// fecha a conex�o
		fecharConexao();
	}
}
