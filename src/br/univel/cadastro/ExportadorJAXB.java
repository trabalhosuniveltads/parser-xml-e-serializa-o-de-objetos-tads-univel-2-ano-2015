package br.univel.cadastro;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class ExportadorJAXB implements Exportador {
	@Override
	public void exportar(ArrayList<Cadastro> c) {

		// gera um objeto e adiciona a lista de objetos passados em "c"
		Cadastros cadastros = new Cadastros();
		// seta a lista para o valor de cadastos
		cadastros.setCadastros(c);

		// gera uma StringWriter "out"
		final StringWriter out = new StringWriter();

		try {
			// gera o contexto.
			JAXBContext context = JAXBContext.newInstance(Cadastros.class,
					Cadastro.class);
			// cria uma vari�vel para conter o marshaller
			Marshaller marshaller = context.createMarshaller();
			// seta o cabe�alho
			marshaller.setProperty(
					javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT,
					Boolean.TRUE);
			// gera o resultado do marshaller para "out"
			marshaller.marshal(cadastros, new StreamResult(out));

			// cria um novo arquivo: c:\cadastros.xml
			File file = new File("c:/cadastros.xml");
			// grava o conte�do do arquivo
			marshaller.marshal(cadastros, file);

		} catch (JAXBException e) {
			// TODO tratar depois
			e.printStackTrace();
		}

	}

	public Object importar() {
		Cadastros cadastros = new Cadastros();

		try {
			JAXBContext context = null;
			context = JAXBContext.newInstance(Cadastros.class, Cadastro.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			cadastros = (Cadastros) unmarshaller.unmarshal(new StreamSource(
					new FileInputStream("c:/cadastros.xml")));
		} catch (JAXBException e) {
			// mensagem de erro
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return cadastros;

	}

}
