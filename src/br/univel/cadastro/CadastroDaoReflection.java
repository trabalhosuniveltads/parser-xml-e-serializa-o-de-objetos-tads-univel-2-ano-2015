package br.univel.cadastro;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CadastroDaoReflection implements CadastroDao {
	private Connection con;

	@Override
	public void abrirConexao() throws SQLException {
		String url = "jdbc:h2:~/javacadastro";
		String user = "sa";
		String pass = "sa";
		con = DriverManager.getConnection(url, user, pass);
	}
	
	@Override
	public void fecharConexao() throws SQLException {
		// encerra a conex�o
		con.close();
	}

	@Override
	public void criar(Cadastro c) throws SQLException{	
		//abrir conexao
		abrirConexao();
				
		//array com os atributos do da classe Cadastro  
		Field[] atributos = c.getClass().getDeclaredFields();
		
		// le a classe Cadastro (c) e recupera seu NOME
		String nome_da_tabela  = c.getClass().getSimpleName().toUpperCase();
		
		// concatenamos os textoc com StringBuilder
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO " + nome_da_tabela  + " (");
		int atrib_validos = 0;
		//coleta os atributos
		for (int i = 0; i < atributos.length; i++) {
			if (atributos[i].isAnnotationPresent(Ann_Cadastro.class)) {
				if (atrib_validos > 0) {
					sb.append(", ");
				}
				atributos[i].setAccessible(true);
				sb.append(atributos[i].getName().toUpperCase());
				atrib_validos++;
			}
		}
		// concatena textos
		sb.append(") VALUES (");
		// contar as vari�veis
		atrib_validos = 0;
		for (int i = 0; i < atributos.length; i++) {
			atributos[i].setAccessible(true);
			if (atributos[i].isAnnotationPresent(Ann_Cadastro.class)) {
				if (atrib_validos > 0) {
					sb.append(", ");
				}
				sb.append("?");
				atrib_validos++;
			}
		}
		//finaliza a instru��o sql
		sb.append(")");

		// o comando est� gerado
		String comando_sql = sb.toString();
		
		//imprimir na tela a instru��o sql
		System.out.println(comando_sql);
		
		// Use essa sql no PreparedStatement Ex:
		PreparedStatement ps = con.prepareStatement(comando_sql);
		
		// Atribua os valores dessa forma:
		atrib_validos=0;
		try{
			for (int i = 0; i < atributos.length; i++) {
				if (atributos[i].isAnnotationPresent(Ann_Cadastro.class)) {
					atrib_validos++;
					ps.setObject(atrib_validos, atributos[i].get(c));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch ( IllegalArgumentException e){
			e.printStackTrace();
		}catch ( IllegalAccessException e ){
			e.printStackTrace();
		}
		ps.executeUpdate();
		
		// close
		ps.close();
		// encerra conex�o
		fecharConexao();
	}

	@Override
	public List<Cadastro> ler() throws SQLException {
		List<Cadastro> lista = new ArrayList<Cadastro>();

		// gera um objeto para coletar a classe....
		Cadastro c = new Cadastro();
		
		//abrir conexao
		abrirConexao();
		
		// le a classe Cadastro (c) e recupera seu NOME
		String nome_da_tabela  = c.getClass().getSimpleName().toUpperCase();
		
		// gera um estatement para a conexao
		Statement st = con.createStatement();
		// coleta os resultados
		ResultSet result = st.executeQuery("SELECT * FROM " + nome_da_tabela  + "; ");
		
		//percorre os resultados
		while (result.next()) {
			//coleta os dados dos resultados em um modelo Cadastro
			Cadastro x = new Cadastro(result.getInt(1), result.getString(2), result.getString(3), result.getString(4));
			//adiciona o modelo na lista
			lista.add(x);
		}
		
		// encerra conex�o
		fecharConexao();

		return lista;
	}

	@Override
	public void atualizar(Cadastro c) throws SQLException {
		//abrir conexao
		abrirConexao();
				
		//array com os atributos do da classe Cadastro  
		Field[] atributos = c.getClass().getDeclaredFields();
		
		// le a classe Cadastro (c) e recupera seu NOME
		String nome_da_tabela  = c.getClass().getSimpleName().toUpperCase();
		
		// concatenamos os textoc com StringBuilder
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE " + nome_da_tabela  + " SET ");
		int atrib_validos = 0;
		//coleta os atributos
		for (int i = 0; i < atributos.length; i++) {
			if (atributos[i].isAnnotationPresent(Ann_Cadastro.class)) {
				if (atrib_validos > 0) {
					sb.append(", ");
				}
				atributos[i].setAccessible(true);
				sb.append(atributos[i].getName().toUpperCase());
				sb.append(" = ?");
				atrib_validos++;
			}
		}
				
		// concatena textos
		sb.append("WHERE id = " + c.getId());
		
		// o comando est� gerado
		String comando_sql = sb.toString();
		
		//imprimir na tela a instru��o sql
		System.out.println(comando_sql);
		
		// Use essa sql no PreparedStatement Ex:
		PreparedStatement ps = con.prepareStatement(comando_sql);
		
		// Atribua os valores dessa forma:
		atrib_validos=0;
		try{
			for (int i = 0; i < atributos.length; i++) {
				if (atributos[i].isAnnotationPresent(Ann_Cadastro.class)) {
					atrib_validos++;
					ps.setObject(atrib_validos, atributos[i].get(c));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch ( IllegalArgumentException e){
			e.printStackTrace();
		}catch ( IllegalAccessException e ){
			e.printStackTrace();
		}
		ps.executeUpdate();
		
		// close
		ps.close();
		// encerra conex�o
		fecharConexao();
		
	}

	@Override
	public void deletar(Cadastro c) throws SQLException {
		//abrir conexao
		abrirConexao();
		
		// le a classe Cadastro (c) e recupera seu NOME
		String nome_da_tabela  = c.getClass().getSimpleName().toUpperCase();
		
		// concatenamos os textoc com StringBuilder
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM " + nome_da_tabela  + " WHERE id = " + c.getId());
		
		// o comando est� gerado
		String comando_sql = sb.toString();
		
		//imprimir na tela a instru��o sql
		System.out.println(comando_sql);
		
		// Use essa sql no PreparedStatement Ex:
		PreparedStatement ps = con.prepareStatement(comando_sql);
		
		ps.executeUpdate();
		
		// close
		ps.close();
		// encerra conex�o
		fecharConexao();
		
	}

}
