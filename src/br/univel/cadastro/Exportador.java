package br.univel.cadastro;

import java.util.ArrayList;

public interface Exportador {
	
	public void exportar(ArrayList<Cadastro> c);
	public Object importar();

}
