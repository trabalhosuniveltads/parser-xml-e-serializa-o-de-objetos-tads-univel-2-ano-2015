package br.univel.cadastro;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class ModCadastro extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private List<Cadastro> l = new ArrayList<Cadastro>();
	
	@Override
	public int getColumnCount() { return 4; }

	@Override
	public int getRowCount() { return l.size(); }
	
	public void clear() { l.clear(); }	

	@Override
	public Object getValueAt(int row, int col) {

		Cadastro c = l.get(row);
		switch (col) {
		case 0:
			return c.getId();
		case 1:
			return c.getNome();
		case 2:
			return c.getTelefone();
		case 3:
			return c.getEndereco();
		default:
			return "Erro";
		}
	}

	public void incluir (Cadastro c){
		l.add(c);
		super.fireTableDataChanged();
	}

	@Override
	public String getColumnName (int col) {
		switch (col) {
		case 0:
			return "Id";
		case 1:
			return "Nome";
		case 2:
			return "Telefone";
		case 3:
			return "Endere�o";
		default:
			return "Erro";
		}
	}
}