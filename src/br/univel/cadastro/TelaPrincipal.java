package br.univel.cadastro;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;

import javax.swing.JTextField;

import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.xml.bind.JAXBException;

import br.univel.cadastro.ExportadorJAXB;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


public class TelaPrincipal extends JFrame {
	
	// v�ri�veis de uso da aplica��o
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txf_id;
	private JTextField txf_nome;
	private JTextField txf_telefone;
	private JTextField txf_endereco;
	private JTable table;
	private ModCadastro modelo;

	// 
	
	// CadastroDAO ou CadastroDAOReflection voc� decide	
	//CadastroDaoSimples cdao = new CadastroDaoSimples();
	CadastroDaoReflection cdao = new CadastroDaoReflection();

	protected void Configuracoes() {
		// instancia do ModCadastro
		modelo = new ModCadastro();
		// seta o modelo na tabela 
		table.setModel(modelo);
	}
	
	// Criando o frame
	public TelaPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 580, 467);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 83, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblNewLabel = new JLabel("ID");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);
		
		txf_id = new JTextField();
		GridBagConstraints gbc_txf_id = new GridBagConstraints();
		gbc_txf_id.gridwidth = 5;
		gbc_txf_id.insets = new Insets(0, 0, 5, 0);
		gbc_txf_id.fill = GridBagConstraints.HORIZONTAL;
		gbc_txf_id.gridx = 1;
		gbc_txf_id.gridy = 0;
		contentPane.add(txf_id, gbc_txf_id);
		txf_id.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Nome");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 1;
		contentPane.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		txf_nome = new JTextField();
		GridBagConstraints gbc_txf_nome = new GridBagConstraints();
		gbc_txf_nome.gridwidth = 5;
		gbc_txf_nome.insets = new Insets(0, 0, 5, 0);
		gbc_txf_nome.fill = GridBagConstraints.HORIZONTAL;
		gbc_txf_nome.gridx = 1;
		gbc_txf_nome.gridy = 1;
		contentPane.add(txf_nome, gbc_txf_nome);
		txf_nome.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Telefone");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 2;
		contentPane.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		txf_telefone = new JTextField();
		GridBagConstraints gbc_txf_telefone = new GridBagConstraints();
		gbc_txf_telefone.gridwidth = 5;
		gbc_txf_telefone.insets = new Insets(0, 0, 5, 0);
		gbc_txf_telefone.fill = GridBagConstraints.HORIZONTAL;
		gbc_txf_telefone.gridx = 1;
		gbc_txf_telefone.gridy = 2;
		contentPane.add(txf_telefone, gbc_txf_telefone);
		txf_telefone.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Endere\u00E7o");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 3;
		contentPane.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		txf_endereco = new JTextField();
		GridBagConstraints gbc_txf_endereco = new GridBagConstraints();
		gbc_txf_endereco.gridwidth = 5;
		gbc_txf_endereco.insets = new Insets(0, 0, 5, 0);
		gbc_txf_endereco.fill = GridBagConstraints.HORIZONTAL;
		gbc_txf_endereco.gridx = 1;
		gbc_txf_endereco.gridy = 3;
		contentPane.add(txf_endereco, gbc_txf_endereco);
		txf_endereco.setColumns(10);
		
		JButton btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// a��o de incluir
				try {
					acCreate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
					
			}
		});
		
		JButton btnExportar = new JButton("exportar XML");
		btnExportar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// a��o de exportar para Xml
				try {
					acExportarXml();
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (JAXBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		JButton btnExportarSer = new JButton("exportar SER");
		btnExportarSer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// a��o de exportar para SER
				try {
					acExportarSer();
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		GridBagConstraints gbc_btnExportarSer = new GridBagConstraints();
		gbc_btnExportarSer.insets = new Insets(0, 0, 5, 5);
		gbc_btnExportarSer.gridx = 0;
		gbc_btnExportarSer.gridy = 4;
		contentPane.add(btnExportarSer, gbc_btnExportarSer);
		
		JButton btnImportarSer = new JButton("importar SER");
		btnImportarSer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					acImportarSer();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}				
			}
		});
		GridBagConstraints gbc_btnImportarSer = new GridBagConstraints();
		gbc_btnImportarSer.insets = new Insets(0, 0, 5, 5);
		gbc_btnImportarSer.gridx = 1;
		gbc_btnImportarSer.gridy = 4;
		contentPane.add(btnImportarSer, gbc_btnImportarSer);
		GridBagConstraints gbc_btnExportar = new GridBagConstraints();
		gbc_btnExportar.insets = new Insets(0, 0, 5, 5);
		gbc_btnExportar.gridx = 0;
		gbc_btnExportar.gridy = 5;
		contentPane.add(btnExportar, gbc_btnExportar);
		
		JButton btnImportarXml = new JButton("importar XML");
		btnImportarXml.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					acImportarXml();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		GridBagConstraints gbc_btnImportarXml = new GridBagConstraints();
		gbc_btnImportarXml.insets = new Insets(0, 0, 5, 5);
		gbc_btnImportarXml.gridx = 1;
		gbc_btnImportarXml.gridy = 5;
		contentPane.add(btnImportarXml, gbc_btnImportarXml);
		btnCreate.setHorizontalAlignment(SwingConstants.RIGHT);
		btnCreate.setVerticalAlignment(SwingConstants.BOTTOM);
		GridBagConstraints gbc_btnCreate = new GridBagConstraints();
		gbc_btnCreate.anchor = GridBagConstraints.EAST;
		gbc_btnCreate.insets = new Insets(0, 0, 5, 5);
		gbc_btnCreate.gridx = 2;
		gbc_btnCreate.gridy = 5;
		contentPane.add(btnCreate, gbc_btnCreate);
		
		JButton btnRead = new JButton("Read");
		btnRead.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					// a��o de ler
					acRead();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		
		GridBagConstraints gbc_btnRead = new GridBagConstraints();
		gbc_btnRead.insets = new Insets(0, 0, 5, 5);
		gbc_btnRead.gridx = 3;
		gbc_btnRead.gridy = 5;
		contentPane.add(btnRead, gbc_btnRead);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				try {
					// a��o de atualizar
					acUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		
		GridBagConstraints gbc_btnUpdate = new GridBagConstraints();
		gbc_btnUpdate.insets = new Insets(0, 0, 5, 5);
		gbc_btnUpdate.gridx = 4;
		gbc_btnUpdate.gridy = 5;
		contentPane.add(btnUpdate, gbc_btnUpdate);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				try {
					// a��o de deletar
					acDelete();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		
		GridBagConstraints gbc_btnDelete = new GridBagConstraints();
		gbc_btnDelete.insets = new Insets(0, 0, 5, 0);
		gbc_btnDelete.gridx = 5;
		gbc_btnDelete.gridy = 5;
		contentPane.add(btnDelete, gbc_btnDelete);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 6;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 6;
		contentPane.add(scrollPane, gbc_scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton button = new JButton("exportar");
		scrollPane.setColumnHeaderView(button);
	}

	protected void acCreate() throws SQLException {

		//coletar os dados dos campos de textos para vari�veis
		
		// garante que o id seja um numero inteiro v�lido
		int id = 0;
		try{
			id = Integer.parseInt(txf_id.getText().trim());			
		} catch (Exception e){
			JOptionPane.showMessageDialog(this, "Id Inv�lido!");
			return;
		}
		
		String nome = txf_nome.getText().trim();
		String telefone = txf_telefone.getText().trim();
		String endereco = txf_endereco.getText().trim();
	
		// cria um novo registro no banco de dados
		cdao.criar(new Cadastro(id, nome, telefone, endereco));
		
		limparTodosOsCampos();
		
		// le o conte�do do banco de dados
		acRead();
		
		JOptionPane.showMessageDialog(this, "Opera��o realizada com sucesso!");
	}
	
	protected void acRead() throws SQLException {
		//limpara a lista da tabela
		modelo.clear();
		// corre o resultado do que foi resgatado do banco de dados
		for (Cadastro c : cdao.ler()) { 
			modelo.incluir(c);
		}
	}
	
	protected void acUpdate() throws SQLException {
		
		//coletar os dados dos campos de textos para vari�veis
		
		// garante que o id seja um numero inteiro v�lido
		int id = 0;
		try{
			id = Integer.parseInt(txf_id.getText().trim());			
		} catch (Exception e){
			JOptionPane.showMessageDialog(this, "Id Inv�lido!");
			return;
		}
		
		String nome = txf_nome.getText().trim();
		String telefone = txf_telefone.getText().trim();
		String endereco = txf_endereco.getText().trim();
	
		// envia os dados para ser atualizado no Banco de dados...
		// s� vai ser atualizado se o id existir... se n�o existir nao vai fazer nada.
		cdao.atualizar(new Cadastro(id, nome, telefone, endereco));
		
		limparTodosOsCampos();
		// le o conte�do do banco de dados
		acRead();
		
	}
	
	protected void acDelete() throws SQLException {
		
		// garante que o id seja um numero inteiro v�lido
		int id = 0;
		try{
			id = Integer.parseInt(txf_id.getText().trim());			
		} catch (Exception e){
			JOptionPane.showMessageDialog(this, "Id Inv�lido!");
			return;
		}
		
		// envia os dados para ser deletado o registro no Banco de dados...
		// s� vai ser atualizado se o id existir... se n�o existir nao vai fazer nada.
		cdao.deletar(new Cadastro(id, null, null, null));
		
		limparTodosOsCampos();
		// le o conte�do do banco de dados
		acRead();

	}

	protected void acExportarXml() throws SQLException, JAXBException, IOException   {
		
		//instancia o exportador xml
		ExportadorJAXB xml = new ExportadorJAXB();
		
		// variavel temporaria para armazenar o xml
		xml.exportar((ArrayList) cdao.ler());
		
	}
	
	protected void acExportarSer() throws IOException, SQLException   {
		
		//instancia o exportador ser
		ExportadorSER ser = new ExportadorSER();
		
		// exportar
		ser.exportar((ArrayList) cdao.ler());
	}
	

	protected void acImportarSer() throws IOException, SQLException   {
		
		//instancia o exportador ser
		ExportadorSER ser = new ExportadorSER();
		ArrayList<Cadastro> imp = (ArrayList<Cadastro>) ser.importar();
		
		// cria uma lista do banco
		ArrayList<Cadastro> lista = new ArrayList<Cadastro>();
		try {
			// corre a lista importada
			for (Cadastro regSER : imp) {
				//variavel temporaria
				boolean duplicado = false;
				// busca os registros que estao no banco de dados (fresquinhos)
				lista = (ArrayList) cdao.ler();	
				// corre o conte�do do banco de dados e verifica se o registro ja se encontra cadastrado 	
				for (Cadastro regDB : lista) {
					if (regSER.getId() == regDB.getId()) {
						duplicado = true;
					}
				}
				// atualiza ou cria um regiustro novo
				if (duplicado) {
					cdao.atualizar(regSER);
				} else {
					cdao.criar(regSER);
				}
			}
			// atualiza a lista na tabela
			acRead();
		
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	protected void acImportarXml() throws IOException, SQLException   {
		
		//instancia o exportador ser
		ExportadorSER ser = new ExportadorSER();
		ArrayList<Cadastro> imp = (ArrayList<Cadastro>) ser.importar();
		
		// cria uma lista do banco
		ArrayList<Cadastro> lista = new ArrayList<Cadastro>();
		try {
			// corre a lista importada
			for (Cadastro regSER : imp) {
				//variavel temporaria
				boolean duplicado = false;
				// busca os registros que estao no banco de dados (fresquinhos)
				lista = (ArrayList) cdao.ler();	
				// corre o conte�do do banco de dados e verifica se o registro ja se encontra cadastrado 	
				for (Cadastro regDB : lista) {
					if (regSER.getId() == regDB.getId()) {
						duplicado = true;
					}
				}
				// atualiza ou cria um regiustro novo
				if (duplicado) {
					cdao.atualizar(regSER);
				} else {
					cdao.criar(regSER);
				}
			}
			// atualiza a lista na tabela
			acRead();
		
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	
	
	
	
	
	
	
	private void limparTodosOsCampos() {
		txf_id.setText("");
		txf_nome.setText("");
		txf_telefone.setText("");
		txf_endereco.setText("");
	}


	
	
	
	
	
	//M�todo MAIN	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					// instancia da Aplica��o
					TelaPrincipal app = new TelaPrincipal();
					
					// Setando as configura��es
					app.Configuracoes();
					
					// mostrando a aplica��o
					app.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}
	
}
