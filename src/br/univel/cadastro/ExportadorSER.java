package br.univel.cadastro;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class ExportadorSER implements Exportador{
	@Override
	public void exportar(ArrayList<Cadastro> c){
		try {
			// TODO Auto-generated method stub
	        FileOutputStream file = new FileOutputStream("c:/cadastros.ser");
	        ObjectOutputStream oos = new ObjectOutputStream(file);
	        oos.writeObject(c);
	        oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	
	
	
	public Object importar() {
		
		Object cadastros = null;
		
		try {
			FileInputStream file = new FileInputStream("c:/cadastros.ser");
	        ObjectInputStream ois = new ObjectInputStream(file);
	        cadastros = ois.readObject();
	        ois.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return cadastros;
		
        
	}

}
