# TADS UNIVEL 2015 #

Trabalho de Orientação a Objetos para referência, desenvolvido para a disciplina de TADS (2º ANO) da Faculdade de Ciências Sociais Aplicadas de Cascavel (UNIVEL) no ano de 2015.

### Descrição do trabalho ###

**ATENÇÃO**: CRIE O PROJETO COM O NOME CONTENDO SEU RA, EX: RA290302-2bim

Crie uma aplicação completa com Java e interface Swing para cadastro de Contato. Essa classe deve possuir os seguintes atributos: id, nome, telefone, endereco.

Crie duas implementações para persistência em banco H2, uma com os SQLs hard coded na classe e outra que gera os SQL com base em Annotations customizadas.

Crie também um exportador. Para isso defina interface e 2 implementações: Jaxb e Serialização (swiiiii!). Esse exportador deve funcionar como backup, então faça também o importador.

**CRITÉRIO DE AVALIAÇÃO:**

O ALUNO QUE ENTREGAR NA DATA RECEBE 1 DÉCIMO E O DIREITO DE APRESENTAR PESSOALMENTE. NA APRESENTAÇÃO INDIVIDUAL O ALUNO PODE GANHAR OS 1,9 PONTOS RESTANTES SE SOUBER RESPONDER AS PERGUNTAS DO PROFESSOR (DE 3 A 5).

TODO O CÓDIGO DEVE ESTAR COMENTADO, NO MÍNIMO A CLASSE E OS MÉTODOS.

**ROTEIRO:**
- O ALUNO BAIXA SEU TRABALHO DO AVA.
- O ALUNO CADASTRA DOIS CONTATOS.
- O ALUNO ALTERA UM DOS CONTATOS E APAGA O OUTRO.
- O ALUNO CADASTRA MAIS DOIS CONTATOS.
- O ALUNO EXPORTA O QUE TEM NO BANCO PARA XML.
- O ALUNO ABRE O XML NO NOTEPAD++.